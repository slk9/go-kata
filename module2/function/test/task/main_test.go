package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGreeting(t *testing.T) {
	expectedResult := "Hello"
	receivedResult := Greeting()
	assert.Equal(t, expectedResult, receivedResult)
}

func TestGoodBye(t *testing.T) {
	expectedResult := "Goodbye,Tom. . ."
	receivedResult := GoodBye("Tom")
	assert.Equal(t, expectedResult, receivedResult)
}

func TestAdd(t *testing.T) {
	type testingAdd struct {
		a      int
		b      int
		result int
	}

	testArray := []testingAdd{
		{a: 4, b: 6, result: 10},
		{a: 2, b: 4, result: 6},
	}

	for _, test := range testArray {
		expectedResult := test.result
		receivedResult := Add(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf(`
Значение а: %d  
Значение b: %d
Ожидаемый результат результат работы программы: %d    
Фактический результат работы программы: %d
`, test.a, test.b, expectedResult, receivedResult)
	}

}
