package main

import "fmt"

func main() {
	fmt.Println(Greeting())
	fmt.Println(GoodBye("Tom"))
}

func Greeting() string {
	return "Hello"
}

func GoodBye(name string) string {
	text := "Goodbye," + name + ". . ."
	return text
}

func Add(a int, b int) int {
	return a + b
}
