package main

import (
	"fmt"
	"unicode"
)

func main() {
	fmt.Println(Greet("не джон"))
}

func Greet(name string) string {
	distribution := IsEngByLoop(name)
	var a string
	if distribution {
		a = "Hello " + name + ", you welcome!"
	} else {
		a = "Привет " + name + ", добро пожаловать!"
	}
	return a

}

func IsEngByLoop(str string) bool {
	for i := 0; i < len(str); i++ {
		if str[i] > unicode.MaxASCII {
			return false
		}
	}
	return true
}
