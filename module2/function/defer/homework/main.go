package main

import (
	"errors"
	"fmt"
)

var (
	errNotEnoughDicts = errors.New("Требуемое количество словарей, не менее 2-х")
	errNilDict        = errors.New("словарь пуст")
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

func main() {

	resjob, err := ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}})

	if err != nil {
		fmt.Printf(`Ошибка в процессе выполнения: %s 
Обзор ошибки: %q
`, err, resjob.Dicts)
	} else {
		fmt.Println(resjob)
	}
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {

	job.Merged = make(map[string]string)

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	} else {
		for _, dicts := range job.Dicts {
			if dicts == nil {
				return job, errNilDict
			} else {
				for key, value := range dicts {
					job.Merged[key] = value
				}

			}
		}

	}

	defer True(job)

	return job, nil
}

func True(job *MergeDictsJob) *MergeDictsJob {
	job.IsFinished = true
	return job
}
