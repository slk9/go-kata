package main

import "testing"

func BenchmarkSimples(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkSimples2(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}
