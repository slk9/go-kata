package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	c := &s
	Append(c)
	fmt.Println(s, cap(s))
}

func Append(arr *[]int) {
	*arr = append(*arr, 4)

}
