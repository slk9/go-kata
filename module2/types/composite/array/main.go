package main

import "fmt"

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	testArray()
}

func testArray() {
	users := [4]User{
		{
			Age:  21,
			Name: "Vyacheslav",
			Wallet: Wallet{
				RUB: 7000,
				USD: 100,
				BTC: 2,
				ETH: 1,
			},
		},
		{
			Age:  21,
			Name: "Alexander",
			Wallet: Wallet{
				RUB: 0,
				USD: 0,
				BTC: 4,
				ETH: 2,
			},
		},
		{
			Age:  14,
			Name: "Egor",
			Wallet: Wallet{
				RUB: 21000,
				USD: 300,
				BTC: 6,
				ETH: 3,
			},
		},
		{
			Age:  42,
			Name: "Olga",
			Wallet: Wallet{
				RUB: 28000,
				USD: 400,
				BTC: 0,
				ETH: 0,
			},
		},
	}

	fmt.Println("Совершенно летние пользователи:")
	for i := range users {
		if users[i].Age > 18 {
			fmt.Println(users[i])
		}
	}

	fmt.Println("\nБаланс криптовалютного кошелька пуст:")
	for i, user := range users {
		if users[i].Wallet.BTC == 0 || users[i].Wallet.ETH == 0 {
			fmt.Println("user:", user, "index:", i)
		}
	}

	fmt.Println("\nБаланс валютного кошелька пуст:")
	for i, user := range users {
		if users[i].Wallet.RUB == 0 || users[i].Wallet.USD == 0 {
			fmt.Println("user:", user, "index:", i)
		}
	}

	a := [...]int{10, 20, 40, 80}

	fmt.Println("\nOriginal value:", a)

	a[0] = 15

	fmt.Println("\nChanged first element:", a)

	b := a
	a[0] = 20

	fmt.Println("\aOriginal array:", a)

	fmt.Println("\nModified array value:", b)
}
