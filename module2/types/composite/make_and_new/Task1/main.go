package main

import "fmt"

func main() {
	n := new(int)
	fmt.Printf(`Значение переменной n = %d
Тип переменной n: %T 
`, *n, n)
	*n = 5
	fmt.Printf(`Значение переменной n = %d
Тип переменной n: %T 
`, *n, n)
}
