package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{

		{
			Name:  "https://github.com/docker/compose",
			Stars: 28400,
		},
		{
			Name:  "https://github.com/nicksieger/tilt-custom-extensions",
			Stars: 1,
		},
		{
			Name:  "https://github.com/ossu/computer-science",
			Stars: 131000,
		},
		{
			Name:  "https://github.com/slidevjs/slidev",
			Stars: 24500,
		},
		{
			Name:  "https://github.com/jlevy/the-art-of-command-line",
			Stars: 2800,
		},
		{
			Name:  "https://github.com/python-telegram-bot/python-telegram-bot",
			Stars: 20800,
		},
		{
			Name:  "https://github.com/twbs/bootstrap",
			Stars: 161000,
		},
		{
			Name:  "https://github.com/animate-css/animate.css",
			Stars: 76900,
		},
		{
			Name:  "https://github.com/sindresorhus/awesome",
			Stars: 236000,
		},
		{
			Name:  "https://github.com/getify/You-Dont-Know-JS",
			Stars: 164000,
		},
		{
			Name:  "https://github.com/ohmyzsh/ohmyzsh",
			Stars: 155000,
		},
		{
			Name:  "https://github.com/EbookFoundation/free-programming-books",
			Stars: 263000,
		},
		{
			Name:  "https://github.com/chubin/cheat.sh",
			Stars: 34500,
		},
	}

	// ЗАДАНИЕ 1: в цикле запишите в map_and_valueType

	Map := make(map[string]Project)
	for i := 0; i < len(projects); i++ {
		Map[projects[i].Name] = projects[i]
	}
	fmt.Print(`1 задание:
`, Map)

	//ЗАДАНИЕ 2: в цикле пройдитесь по мапу и выведите значения в консоль
	fmt.Print(`

2 задание:
`)
	for key := range Map {
		fmt.Println(key)
	}

}
