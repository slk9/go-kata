package main

import (
	"fmt"
)

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch Aboba := r.(type) {
	case *int:
		if r.(*int) == nil {
			fmt.Println("Success!")
		}
	default:
		fmt.Printf("Другой тип: %T \n", Aboba)
	}

	if r == nil {
		fmt.Println("Success!")
	}
}
