package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	fmt.Println(u.Name)
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	var i Userer = &User{}
	_ = i
	fmt.Println("Success!")
}
