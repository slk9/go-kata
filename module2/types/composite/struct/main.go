package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Location struct {
	Address string
	City    string
	index   string
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	user := User{
		Age:  43,
		Name: "Alexander",
	}
	wallet := Wallet{
		RUB: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	Location := Location{
		Address: "ул.Заводская, д.2/1, кв.78",
		City:    "Саратов",
		index:   "412163",
	}

	user.Wallet = wallet
	user.Location = Location
	fmt.Println(user)
	fmt.Println(wallet)

	fmt.Println(`"walled allocates"`, unsafe.Sizeof(wallet), `/\bytes/\`)
}
