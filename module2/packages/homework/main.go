package main

import (
	"fmt"

	"gitlab.com/slk9/greet"
)

func main() {
	greet.Hello()

	fmt.Println(greet.Shark)

	oct := greet.Octopus{
		Name:  "Колин",
		Color: "красный",
	}
	fmt.Println(oct.String())
}
