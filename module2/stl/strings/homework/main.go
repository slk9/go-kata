package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Cache struct {
	data map[string]User
}

func NewCache() Cache {
	return Cache{data: make(map[string]User, 100)}
}

func (c Cache) Set(key string, u User) Cache {
	c.data[key] = u
	return c
}

func (c Cache) Get(key string) User {
	return c.data[key]
}

type User struct {
	ID       int
	Nickname string
	Email    string
}

func main() {
	var users []User
	emails := []string{"robpike@gmail.com", "davecheney@gmail.com", "bradfitzpatrick@email.ru", "eliben@gmail.com", "quasilyte@mail.ru"}

	eml := make([]string, len(emails))
	for key, value := range emails {
		_ = key
		slk := strings.Split(value, "@")
		eml[key] = slk[0]

	}

	for i := range emails {
		users = append(users, User{
			ID:       i + 1,
			Nickname: eml[i],
			Email:    emails[i],
		})

	}
	cache := NewCache()
	for key, value := range users {
		_ = key
		k := strconv.Itoa(value.ID)
		s := strings.Join([]string{value.Nickname, k}, ":")
		cache.Set(s, users[key])

	}
	keys := []string{"robpike:1", "davecheney:2", "bradfitzpatrick:3", "eliben:4", "quasilyte:5"}
	for i := range keys {
		fmt.Println(cache.Get(keys[i]))
	}
	//Если не сложно можете ответить на вопрос?/
	//Верно ли моё решение задачи, учитывая
	//факт того, что я полностью убрал указатели и заменивих на return?
	//Воспрос основан на факте того, что указатели съедают прозводительность, и выглядит нелогичным их
	//использование без острой надобности
}
