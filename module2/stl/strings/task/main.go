package main

import (
	"fmt"
	"strings"
)

func main() {
	name := "Samy Loverson"

	fmt.Println("Вывод имени заглавными:", strings.ToUpper(name))

	fmt.Println("Вывод имени строчными:", strings.ToLower(name))

	fmt.Println("Строка начинается с Samy? \nОтвет:", strings.HasPrefix(name, "Samy"))

	fmt.Println("Строка заканчивается на Loverson? \nОтвет:", strings.HasSuffix(name, "Loverson"))

	fmt.Println("Содержит ли строка последовательность символов am? \nОтвет:", strings.Contains(name, "am"))

	fmt.Println("Сколько раз в строке повторется буква o? \n Ответ:", strings.Count(name, "o"))

	//Подсчёт количесва символов
	var pass string
	fmt.Println("Введите словосочетание длиной не менее 6 символов и не более 12 символов")
	v, err := fmt.Scan(&pass)
	_ = v
	_ = err
	if len(pass) < 6 {
		fmt.Println("Количство символов в пароле менее 6")
	} else if len(pass) > 12 {
		fmt.Println("Количство символов в пароле более 12")
	} else {
		fmt.Println("Пароль успешно введён!")
	}

	//Обьединение строк в одну последовательность
	var first, second, third string
	fmt.Println("Введите имя первого игрока: ")
	j, err := fmt.Scanln(&first)
	_ = j
	_ = err
	fmt.Println("Введите имя второго игрока: ")
	k, err := fmt.Scanln(&second)
	_ = k
	_ = err
	fmt.Println("Введите имя третьего игрока: ")
	s, err := fmt.Scanln(&third)
	_ = s
	_ = err
	fmt.Println("Полный спиок игроков: ", strings.Join([]string{first, second, third}, ","))

	//Разбивание последовательной строки на несколько отдельных строк
	message := "Tom, sell , car"
	f := strings.Split(message, ",")
	fmt.Println(f)
	fmt.Printf("%q\n", f)

}
