package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
)

type Byte []byte

type Config struct {
	AppName    string
	Production bool
}

func ReadingToFile(path string) []byte {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	data := make(Byte, 75)

	_, err = file.Read(data)
	if err == io.EOF {
		fmt.Println(err)
	}

	return data
}

func main() {
	var file string
	flag.StringVar(&file, "file", "", "number of lines to read from the file")
	flag.Parse()

	config := Config{}

	path := "/home/slk/go/src/gitlab.com/slk9/go-kata/module2/stl/flags/homework/" + file

	importJSON := ReadingToFile(path)

	err := json.Unmarshal(importJSON, &config)
	fmt.Println(string(importJSON))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
