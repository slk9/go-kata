package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

type Byte []byte

func (s Byte) String() {
	fmt.Println(string(s))
}

func WritingToFIle(data []string, path string) {
	file, err := os.Create(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	buf := make(Byte, 7)
	for _, k := range data {
		reader := strings.NewReader(k)
		for {
			n, err := reader.Read(buf)
			if err != nil {
				if err == io.EOF {
					fmt.Println(string(buf[:n]))
					break
				}
				fmt.Println(err)
				os.Exit(1)
			}
			//fmt.Println(string(buf[:n])) //создана для провеки заисывемых данных в файл
			g, err := file.Write(buf[:n])
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			_ = g
			w, err := file.Write([]byte("\n"))
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			_ = w

		}
	}
	//fmt.Print("Конец записи текста в файл\n\n") //создана для провеки заисывемых данных в файл
}

func ReadingToFile(path string) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	buffer := make(Byte, 7)

	for {
		_, err := file.Read(buffer)
		if err == io.EOF {
			break
		}

		buffer.String()
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	var name string
	fmt.Print(`
Требуется указать имя файла в который будет сдела запись(без указания .txt)
После успешной записи в терминал ообразть записанная информация с файла
Укажите имя файла:`)
	_, _ = fmt.Scan(&name)
	path := "module2/stl/io/homework/" + name + ".txt"

	WritingToFIle(data, path)
	ReadingToFile(path)

}
