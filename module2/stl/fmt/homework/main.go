package main

import "fmt"

type Person struct {
	name  string
	age   int
	money float64
}

func generateSelfStory(user Person) Person {
	fmt.Printf("\nПривет, меня зовут %s, мне %d, я имею %1.1f рублей на своём валютном кошельке\n ", user.name, user.age, user.money)
	fmt.Printf("\nВведённые вами значения: %v\n", user)
	fmt.Printf("\nВведённые вами значения в таблице: %+v\n", user)
	fmt.Printf("\nПоле содержащее таблицу с вашими значениями: %#v\n", user)
	return user
}

func main() {
	var user Person
	fmt.Println("Введите ваше имя: ")
	s, err := fmt.Scanln(&user.name)
	_ = s
	_ = err
	fmt.Println("Введите ваш возраст: ")
	k, err := fmt.Scanln(&user.age)
	_ = k
	_ = err
	fmt.Println("Введите количество сбережений")
	c, err := fmt.Scanln(&user.money)
	_ = c
	_ = err
	generateSelfStory(user)
}
