package main

import (
	"bufio"
	"fmt"
	"os"
)

func ReadFile(path string) string {
	data, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return string(data)
}

func translate(data string) string {
	runes := []rune(data)
	trslData := ""
	for i := 0; i < len(runes); i++ {

		o := string(runes[i])
		if o == "й" || o == "Й" {
			trslData = trslData + "y"
		} else if o == "ц" || o == "Ц" {
			trslData = trslData + "ts"
		} else if o == "у" || o == "У" {
			trslData = trslData + "u"
		} else if o == "к" || o == "К" {
			trslData = trslData + "k"
		} else if o == "е" || o == "Е" {
			trslData = trslData + "ye"
		} else if o == "н" || o == "Н" {
			trslData = trslData + "n"
		} else if o == "г" || o == "Г" {
			trslData = trslData + "g"
		} else if o == "ш" || o == "Ш" {
			trslData = trslData + "sh"
		} else if o == "щ" || o == "Щ" {
			trslData = trslData + "shch"
		} else if o == "з" || o == "З" {
			trslData = trslData + "z"
		} else if o == "х" || o == "Х" {
			trslData = trslData + "kh"
		} else if o == "ъ" {
			trslData = trslData + ""
		} else if o == "ф" || o == "Ф" {
			trslData = trslData + "f"
		} else if o == "ы" {
			trslData = trslData + "y"
		} else if o == "в" || o == "В" {
			trslData = trslData + "v"
		} else if o == "а" || o == "А" {
			trslData = trslData + "a"
		} else if o == "п" || o == "П" {
			trslData = trslData + "p"
		} else if o == "р" || o == "Р" {
			trslData = trslData + "r"
		} else if o == "о" || o == "О" {
			trslData = trslData + "o"
		} else if o == "л" || o == "Л" {
			trslData = trslData + "l"
		} else if o == "д" || o == "Д" {
			trslData = trslData + "d"
		} else if o == "ж" || o == "Ж" {
			trslData = trslData + "zh"
		} else if o == "э" || o == "Э" {
			trslData = trslData + "ye"
		} else if o == "я" || o == "Я" {
			trslData = trslData + "ya"
		} else if o == "ч" || o == "Ч" {
			trslData = trslData + "ch"
		} else if o == "с" || o == "С" {
			trslData = trslData + "s"
		} else if o == "м" || o == "М" {
			trslData = trslData + "m"
		} else if o == "и" || o == "И" {
			trslData = trslData + "i"
		} else if o == "т" || o == "Т" {
			trslData = trslData + "t"
		} else if o == "ь" {
			trslData = trslData + "'"
		} else if o == "б" || o == "Б" {
			trslData = trslData + "b"
		} else if o == "ю" || o == "Ю" {
			trslData = trslData + "yu"
		} else {
			trslData = trslData + o
		}
	}
	return trslData
}

func WriteToFile(text string, path string) {
	data := []byte(text)
	err := os.WriteFile(path, data, 0777)
	if err != nil {
		panic(err)
	}

}

func AppendToFile(data string) {
	f, err := os.OpenFile("module2/stl/files/write/homework/text.from.console.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = f.WriteString(data)
	if err != nil {
		panic(err)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	AppendToFile(name)

	text := ReadFile("module2/stl/files/write/homework/example.txt") //чтение файла через функцию ReadFile,
	// для чтения файла требуеться передать путь к файлу,
	// как аргумент функции

	trslText := translate(text) //перевод текста в транслит
	//(конетнее кирилицы в транслит)

	WriteToFile(trslText, "module2/stl/files/write/homework/example.processed.txt")
	//запись транслиированного текста в файл example.processed.txt
	//первый аргумент записываемый текст
	//второй аргумен путь к файлу в который будет совершена запись
	//на момент написания кода данного файла не существует, он создаться автоматически поле запуска программы

}
