package main

import (
	"fmt"
	"os"
)

func main() {
	WriteToFile()
	ReadFile()
	AppendToFille()
}

// запись к уже имеющйщейся информации в файле (после этой информации)

func AppendToFille() {
	f, err := os.OpenFile("module2/stl/files/read/task/test.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = f.WriteString(" !")
	if err != nil {
		panic(err)
	}
}

// Замена информаци в файле

func WriteToFile() {
	data := []byte(" Vyacheslav")
	err := os.WriteFile("module2/stl/files/read/task/text.txt", data, 0777)
	if err != nil {
		panic(err)
	}

}

//Простое чтение файла

func ReadFile() {
	data, err := os.ReadFile("module2/stl/files/write/task/test.txt")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(data))
}
