package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {

	// create a buffer to hold JSON data
	buf := new(bytes.Buffer)
	// create JSON encoder for `buf`
	bufEncoder := json.NewEncoder(buf)

	// encode JSON from `Person` structs
	_ = bufEncoder.Encode(Person{"Ross Geller", 28})
	_ = bufEncoder.Encode(Person{"Monica Geller", 27})
	_ = bufEncoder.Encode(Person{"Jack Geller", 56})

	// print contents of the `buf`
	fmt.Println(buf) // calls `buf.String()` method

}
