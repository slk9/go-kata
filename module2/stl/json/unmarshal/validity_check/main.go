package main

import (
	"encoding/json"
	"fmt"
)

func main() {

	// some JSON data
	data := []byte(`
	{
		"FirstName": "John",
		"Age": 21,
		"Username": "johndoe91",
		"Grades": null,
		"Languages": [
		  "English",
		  "French"
		]
	}`)

	// check if `data` is valid JSON
	isValid := json.Valid(data)
	fmt.Println(isValid)
}
