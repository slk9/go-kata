package main

import (
	"encoding/json"
	"fmt"
)

type Studentmap map[string]interface{}

func main() {

	kate := Studentmap{
		"FirstName":      "John",
		"lastName":       "Doe",
		"Age":            21,
		"HeightInMeters": 1.75,
		"IsMale":         true,
	}

	johnJSON3, _ := json.Marshal(kate)
	johnJSON4, _ := json.MarshalIndent(kate, "", "")

	fmt.Println("map_and_valueType", string(johnJSON3))
	fmt.Println("map_and_valueType", string(johnJSON4))

}
