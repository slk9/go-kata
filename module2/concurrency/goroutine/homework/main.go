package main

import (
	"fmt"
	"time"
)

func main() {

	/*go fmt.Println("Goroutine")        //Примитивны программа для поянения устройсва горутин
	go fmt.Println("No goroutine")
	time.Sleep(time.Millisecond)
	*/

	/*t := time.Now()                    //программа показывающа, что разработка кода с горутинами эффективнее
		rand.Seed(t.UnixNano())

		go parseURL("https://moc.baltig/")
		parseURL("https://gitlab.com/")

		fmt.Printf("Parsing completed. Time Elapesed: %.10f seconds\n", time.Since(t).Seconds())
	}

	func parseURL(url string) {

		for i := 0; i < 5; i++ {
			latency := rand.Intn(1000) + 1000

			time.Sleep(time.Duration(latency))

			fmt.Printf("Parsing <%s> - Step %d - Latency %d\n", url, i+1, latency)

		}*/

	/* message := make(chan string)  // Работа по считыванию данных с канала (Канал,единственный способ снятия данных из горутины)

	go func() {
		time.Sleep(2 * time.Second)
		message <- "Hello"
	}()

	msg := <-message
	fmt.Println(msg)*/

	/*	message := make(chan string)          //Использоване небуфиризированного канала (данный способ блокирует горутины в момент записи в канал)

		go func() {
			for i := 0; i <= 10; i++ {
				message <- fmt.Sprintf("%d", i)
				time.Sleep(time.Second + 1)
			}
			close(message)
		}()

		/*for {                                // цикл который получает две переменные с канала, и закрывает канал при возврате false на open
			msg, open := <-message
			if !open {
				break
			}
			fmt.Println(msg)
		}

		for msg := range message {             // синтаксический сахар
			fmt.Println(msg)
		} */

	/*	message := make(chan string, 2) // создание буферизированного канала( в данном случае длина буфера 2
			message <- "hello "  //1
			message <- "world"   //2
			//message <- "world"   //3 deadlock
		    // Но если прочитаь из буфера до запси, которая превышает буфер, то в буфере освободиться место для записи
			fmt.Print(<-message)
			fmt.Print(<-message)*/

	/*	urls := []string{   //пример работы с группой ожидания Ч1/2
			"https://gitlab.com",
			"https://vk.com",
			"https://youtube.com",
		}

		var wg sync.WaitGroup

		for _, url := range urls {
			wg.Add(1)
			go func(url string) {
				doHTTP(url)
				wg.Done()
			}(url)
		}

		wg.Wait() */

	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "прошла половина секунды"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			message2 <- "прошло 2 секунды"
		}
	}()

	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
	}

}

/*func doHTTP(url string) {  //пример работы с группой ожидания Ч2/2
	t := time.Now()

	resp, err := http.Get(url)
	if err !=
nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	fmt.Printf("<%s> - Status Cod [%d] - Latency %d ms\n", url, resp.StatusCode, time.Since(t).Milliseconds())
}
*/
