package main

import "fmt"

type calc struct {
	a, b   float64
	result float64
}

func (c *calc) SetAB(a, b float64) *calc {
	(*c).a = a
	(*c).b = b

	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	(*c).result = operation(c.a, c.b)

	return c
}

func (c calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func divide(a, b float64) float64 {
	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return (a + b) / 2
}

func main() {
	var calc calc
	calcPointer := &calc
	res := calcPointer.SetAB(10, 34).Do(multiply).Result()
	fmt.Printf(`Результаты умножения чисел а(%f)  и b(%f) : %f 
`, calc.a, calc.b, res)
	res = calcPointer.SetAB(5.5, 6).Do(divide).Result()
	fmt.Printf(`Результаты деления чисел а(%f)  и b(%f) : %f 
`, calc.a, calc.b, res)
	res = calcPointer.SetAB(50, 2.45).Do(sum).Result()
	fmt.Printf(`Результаты сложения чисел а(%f)  и b(%f) : %f 
`, calc.a, calc.b, res)
	res = calcPointer.SetAB(4, 5).Do(average).Result()
	fmt.Printf(`Результаты вычисления среднего значения чисел а(%f)  и b(%f) : %f 
`, calc.a, calc.b, res)
	res2 := calc.Result()
	fmt.Println(calc)
	fmt.Println(res2)
	if res != res2 {
		panic("object statement is not persist")
	}
}
