package main

import (
	"testing"
)

func TestMultiply(t *testing.T) {
	tests := []struct {
		name   string
		a, b   float64
		result float64
	}{
		{
			name:   "a:5.5 b:6",
			a:      5.5,
			b:      6,
			result: 33,
		},
		{
			name:   "a:3.5 b:2.5",
			a:      3.5,
			b:      2.5,
			result: 8.75,
		},
		{
			name:   "a:10 b:20",
			a:      10,
			b:      20,
			result: 200,
		},
		{
			name:   "a:4 b:5",
			a:      4,
			b:      5,
			result: 20,
		},
		{
			name:   "a:6 b:5.5",
			a:      6,
			b:      5.5,
			result: 33,
		},
		{
			name:   "a:2.5 b:3.5",
			a:      2.5,
			b:      3.5,
			result: 8.75,
		},
		{
			name:   "a:20 b:10",
			a:      20,
			b:      10,
			result: 200,
		},
		{
			name:   "a:5 b:4",
			a:      5,
			b:      4,
			result: 20,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.a, tt.b); got != tt.result {
				t.Errorf("multipli() = %v, result %v", got, tt.result)
			}
		})

	}
}

func TestDivide(t *testing.T) {
	tests := []struct {
		name   string
		a, b   float64
		result float64
	}{
		{
			name:   "a:5.5 b:6",
			a:      5.5,
			b:      6,
			result: 0.9166666666666666,
		},
		{
			name:   "a:3.5 b:2.5",
			a:      3.5,
			b:      2.5,
			result: 1.4,
		},
		{
			name:   "a:10 b:20",
			a:      10,
			b:      20,
			result: 0.5,
		},
		{
			name:   "a:4 b:5",
			a:      4,
			b:      5,
			result: 0.8,
		},
		{
			name:   "a:6 b:5.5",
			a:      6,
			b:      5.5,
			result: 1.0909090909090908,
		},
		{
			name:   "a:2.5 b:3.5",
			a:      2.5,
			b:      3.5,
			result: 0.7142857142857143,
		},
		{
			name:   "a:20 b:10",
			a:      20,
			b:      10,
			result: 2,
		},
		{
			name:   "a:5 b:4",
			a:      5,
			b:      4,
			result: 1.25,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.a, tt.b); got != tt.result {
				t.Errorf("divide() = %v, result %v", got, tt.result)
			}
		})

	}
}

func TestSum(t *testing.T) {
	tests := []struct {
		name   string
		a, b   float64
		result float64
	}{
		{
			name:   "a:5.5 b:6",
			a:      5.5,
			b:      6,
			result: 11.5,
		},
		{
			name:   "a:3.5 b:2.5",
			a:      3.5,
			b:      2.5,
			result: 6,
		},
		{
			name:   "a:10 b:20",
			a:      10,
			b:      20,
			result: 30,
		},
		{
			name:   "a:4 b:5",
			a:      4,
			b:      5,
			result: 9,
		},
		{
			name:   "a:6 b:5.5",
			a:      6,
			b:      5.5,
			result: 11.5,
		},
		{
			name:   "a:2.5 b:3.5",
			a:      2.5,
			b:      3.5,
			result: 6,
		},
		{
			name:   "a:20 b:10",
			a:      20,
			b:      10,
			result: 30,
		},
		{
			name:   "a:5 b:4",
			a:      5,
			b:      4,
			result: 9,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.a, tt.b); got != tt.result {
				t.Errorf("sum() = %v, result %v", got, tt.result)
			}
		})

	}
}

func TestAverage(t *testing.T) {
	tests := []struct {
		name   string
		a, b   float64
		result float64
	}{
		{
			name:   "a:5.5 b:6",
			a:      5.5,
			b:      6,
			result: 5.75,
		},
		{
			name:   "a:3.5 b:2.5",
			a:      3.5,
			b:      2.5,
			result: 3,
		},
		{
			name:   "a:10 b:20",
			a:      10,
			b:      20,
			result: 15,
		},
		{
			name:   "a:4 b:5",
			a:      4,
			b:      5,
			result: 4.5,
		},
		{
			name:   "a:6 b:5.5",
			a:      6,
			b:      5.5,
			result: 5.75,
		},
		{
			name:   "a:2.5 b:3.5",
			a:      2.5,
			b:      3.5,
			result: 3,
		},
		{
			name:   "a:20 b:10",
			a:      20,
			b:      10,
			result: 15,
		},
		{
			name:   "a:5 b:4",
			a:      5,
			b:      4,
			result: 4.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.a, tt.b); got != tt.result {
				t.Errorf("average() = %v, result %v", got, tt.result)
			}
		})

	}
}
